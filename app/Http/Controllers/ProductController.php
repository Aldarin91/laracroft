<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
{
$data['products'] = Product::orderBy('id','desc')->paginate(5);
return view('products.index', $data);
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/

public function create()
{
return view('products.create');
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$request->validate([
'name' => 'required',
'category' => 'required',
'price' => 'required'
]);
$product = new Product();
$product->name = $request->name;
$product->category = $request->category;
$product->price = $request->price;
$product->save();
return redirect()->route('products.index')
->with('success','Company has been created successfully.');
}
/**
* Display the specified resource.
*
* @param  \App\Product  $company
* @return \Illuminate\Http\Response
*/
}

